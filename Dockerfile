FROM ubuntu

WORKDIR /usr/share/app

RUN apt update && apt -y install sqlite libuuid1

COPY server run.sh ./

CMD ["/usr/share/app/run.sh"]
